package cashapp

import (
	"github.com/gocarina/gocsv"
	"os"
)

type Transaction struct {
	Id          string `csv:"Transaction ID"`
	Date        string `csv:"Date"`
	Type        string `csv:"Transaction Type"`
	Currency    string `csv:"Currency"`
	Amount      string `csv:"Amount"`
	Fee         string `csv:"Fee"`
	NetAmount   string `csv:"Net Amount"`
	AssetType   string `csv:"Asset Type"`
	AssetPrice  string `csv:"Asset Price"`
	AssetAmount string `csv:"Asset Amount"`
	Status      string `csv:"Status"`
	Notes       string `csv:"Notes"`
	Name        string `csv:"Name of sender/receiver"`
	Account     string `csv:"Account"`
}

func Parse(filename string) (transactions []*Transaction, err error) {
	report, err := os.OpenFile(filename, os.O_RDONLY, os.ModePerm)

	if err != nil {
		return nil, err
	}
	defer func(report *os.File) {
		err := report.Close()
		if err != nil {
			// TODO: allow passing in a handler function?
			panic(err)
		}
	}(report)

	transactions = []*Transaction{}

	err = gocsv.UnmarshalFile(report, &transactions)
	if err != nil {
		return nil, err
	}

	return transactions, nil
}
